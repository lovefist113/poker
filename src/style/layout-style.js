import styled from 'styled-components'

const LayoutStyle = styled.div`
  width:100vw;
  min-height:100vh;
  display:flex;
  position:relative;
  background-color:#ddd;
  & .screen-frame{
    width: 375px;
    height: 812px;
    position:absolute;
    top:50px;
    left:50%;
    transform: translate(-50%, 0%);
    box-shadow:1px 2px 8px rgba(0,0,0,.3);
    background: #fff;
  }
  
`

export default LayoutStyle