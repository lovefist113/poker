import styled from 'styled-components'

export const PlaygroundStyle = styled.div`
    background: #000000;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to bottom, #0f9b0f, #000000);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to bottom, #0f9b0f, #000000); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    height:100vh;
    width:100%;

    .card{
        width: 114px;
        height:160px;
        background-size: contain;
        background-position: top left;
    }
    .draw-desk{
        position:absolute;
        top:30px;
        left:30px;
        .get-card{
            float: left;
            display:flex;
            cursor:pointer;
        }
        .reset-circle{
            width:80px;
            height:80px;
            border-radius: 50%;
            background-color:transparent;
            border:10px solid rgba(250,250,250,.6);
            display:block;
            margin:auto;
        }
    }
    .drawed-card{
        margin-left:30px;
        height:160px;
        width: 240px;
        float: right;
        position:relative;

        .card{
            position:absolute;
            top: 0;
        }
    }

    .card-col{
        position:absolute;
        top: 250px;

        &.col-1{
            left: 30px;
        }
        &.col-2{
            left: 180px;
        }
        &.col-3{
            left: 330px;
        }
        &.col-4{
            left: 480px;
        }
        &.col-5{
            left: 630px;
        }
        &.col-6{
            left: 780px;
        }
        &.col-7{
            left: 930px;
        }
        .card{
            position:absolute;
            left:0;
        }
    }

    .archive-cards{
        position:absolute;
        top:30px;
        left:480px;
        width:565px;
        height:160px;
        display:flex;
        justify-content: space-between;

        .archive-position{
            width:114px;
            height:160px;
            border:2px solid rgba(250,250,250,.3);
            border-radius: 6px;
            position:relative;

            .card{
                position:absolute;
                top:0;
                left:0;
            }
        }
    }
`

