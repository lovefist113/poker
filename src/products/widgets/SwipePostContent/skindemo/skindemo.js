import React, { Component } from 'react'
import Slider from "react-slick";
import {SkindemoStyle} from './skindemo.styled'
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

import SliderItem from './SliderItem'


class Skindemo extends Component {
 
  render() {
    const settings = {
      dots: false,
      arrows:false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true,
      variableWidth: true
    };
    return (
      <>
        <SkindemoStyle>
          <Slider {...settings}>
            <SliderItem index="1"/>
            <SliderItem index="2"/>
            <SliderItem index="3"/>
          </Slider>
        </SkindemoStyle>
      </>
    )
  }
}

export default Skindemo
