import React, { Component } from 'react'
import MainContent from './MainContent'
import {ItemStyle} from './skindemo.styled'


export default class SliderItem extends Component {
  constructor(props){
    super(props)
  }
  render() {
    return (
      <ItemStyle>
      <div className="swipe-post-content">
        <MainContent index={this.props.index}/>
      </div>
      </ItemStyle>
    )
  }
}


