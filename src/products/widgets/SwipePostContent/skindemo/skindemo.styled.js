import styled from 'styled-components'

export const SkindemoStyle = styled.div`
  & .slick-slide{
    background:#fff;
    transition:0.3s;
    &:not(.slick-current){
      opacity: 0.4;
      transform: scale(.9)
    }
  }
`

export const ItemStyle = styled.div`
  width: 250px;
  height: 225px;
  border-radius:6px;
  overflow:hidden;
  position:absolute;
  background-color:#fff;
  position:relative;
  background-image:url('https://helpx.adobe.com/content/dam/help/en/stock/how-to/visual-reverse-image-search/jcr_content/main-pars/image/visual-reverse-image-search-v2_intro.jpg');
  background-size:cover;
  background-position:center;
  &:before{
    content:'';
    position:absolute;
    z-index:2;
    width:100%;
    height:50%;
    bottom:0;
    left:0;
    background:linear-gradient(0deg, #000000 0%, rgba(0, 0, 0, 0) 100%);
  }
  & .swipe-post-content{
    display:flex;
    height:100%;
  }
  & .main-content{
    padding:15px;
    display:flex;
    height:100%;
    width:100%;
    flex-direction:column-reverse;
    color:#fff;
    position:relative;
    z-index:3;

    & h3.title{
      font-weight: 500;
      font-size: 16px;
      line-height: 19px;
      color:inherit;
    }

    & .author{
      padding:2px;
      display:flex;
      align-items:center;
      margin-top:10px;

      & .avatar{
        border-radius:50%;
        background-color:#fff;
        background-image:url('https://cdn.tuoitre.vn/thumb_w/586/2019/5/8/avatar-publicitystill-h2019-1557284559744252594756.jpg');
        height:16px;
        width:16px;
        background-size:cover;
        background-position:center;
        margin-right:4px;
      }

      & .name{
        font-size:10px;
        text-transform:uppercase;
        font-weight:500;
        color:inherit;
      }
    }

    & .label{
      display:inline-block;
      position:absolute;
      top:8px;
      left:15px;
      border-radius:10px;
      background-color:#F9A825;
      font-style: normal;
      font-weight: 500;
      font-size: 10px;
      line-height: 12px;
      padding:5px 10px 3px;
      text-align:center;
      text-transform:uppercase;
    }


  }

`

