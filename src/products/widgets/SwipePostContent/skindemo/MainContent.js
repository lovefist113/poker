import React, { Component } from 'react'

export default class MainContent extends Component {
  constructor(props){
    super(props)
  }
  render() {
    return (
      <div className="main-content">
        <div className="meta">
          <h3 className="title">Post Title {this.props.index}</h3>
          <div className="author">
            <div className="avatar"></div>
            <p className="name">Author</p>
          </div>
        </div>
        <div className="label">
          <span>Label</span>
        </div>
      </div>
    )
  }
}
