import React from 'react'
import {useDispatch, useSelector } from 'react-redux'
import {initCardUIinCol} from './gameAction'
import Card from './card'

const Col1 = () => {
    const col6 = useSelector(state => state.col_6_Cards);
    const initCard = initCardUIinCol([...col6])
    return (
        <div className="card-col col-6">
            {initCard.map((card, index) => {
                return <Card key={index} card={card} style={{top: index*30}} dragCol={6} index={index}/>
            })}
        </div>
    )
}

export default Col1