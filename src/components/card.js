import React, {useState} from 'react'
import {useDispatch, useSelector } from 'react-redux'
import {updateColCard} from '../actions/apiAction'
import {moveCard} from './gameAction'
import _ from 'lodash'


const Card = (props) => {
    const {card, style, dragCol, index} = {...props}
    const {flip, name} = card;
    const [dragArr, setDragArr] = useState([]);
    const dispatch = useDispatch();
    const col1 = useSelector(state => state.col_1_Cards);
    const col2 = useSelector(state => state.col_2_Cards);
    const col3 = useSelector(state => state.col_3_Cards);
    const col4 = useSelector(state => state.col_4_Cards);
    const col5 = useSelector(state => state.col_5_Cards);
    const col6 = useSelector(state => state.col_6_Cards);
    const col7 = useSelector(state => state.col_7_Cards);
    const colArr = [col1, col2, col3,col4,col5,col6,col7];
    const styles = {
        backgroundDown: {backgroundImage: 'url("' +process.env.PUBLIC_URL+ '/images/down.png")', ...style},
        backgroundUp: {backgroundImage: 'url("' +process.env.PUBLIC_URL+ '/images/'+name+'.png")',...style},
    }
    
    const onMouseDown = (e) => {
        const removeCol = [...colArr[dragCol - 1]] 
        // push event target (clicked card) and all card below in col into drag container
        setDragArr(removeCol.splice(index,removeCol.length));
        moveCard(e,card,(dropZone) => {
            if(typeof(dropZone) == 'number'){
                // added card col
                const addedCol = [...colArr[dropZone - 1]];
                // removed card col
                const removeCol = [...colArr[dragCol - 1]] 
                // validate drop cart with last card in added col
                const lastCardInCol = _.last(addedCol);
                const canDrop = validateDropCard(card, lastCardInCol);
                // add this card (drop card) to col
                if(canDrop){
                    // add drop card to drop col
                    addedCol.push(card)
                    dispatch(updateColCard(dropZone, addedCol))
                    // remove drop card from drag col
                    removeCol.splice(-1,1)
                    dispatch(updateColCard(dragCol, removeCol))
                }
                // remove all card in dragArr
            }
        })
    }
    console.log(`dragArr`,dragArr)

    const validateDropCard = (dropCard, validateCard) => {
        let valid = false;
        // number should be equal
        if(validateCard.number - dropCard.number === 1 && dropCard.color !== validateCard.color){
            valid = true;
        }

        return valid
    }
    return (
        <div className="card"  style={flip ? styles.backgroundUp : styles.backgroundDown} onMouseDown={flip ? onMouseDown : undefined}>   
        </div>
    )
}

export default Card