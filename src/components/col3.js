import React from 'react'
import {useDispatch, useSelector } from 'react-redux'
import {initCardUIinCol} from './gameAction'
import Card from './card'

const Col1 = () => {
    const col3 = useSelector(state => state.col_3_Cards);
    const initCard = initCardUIinCol([...col3])
    return (
        <div className="card-col col-3">
            {initCard.map((card, index) => {
                return <Card key={index} card={card} style={{top: index*30}} dragCol={3} index={index}/>
            })}
        </div>
    )
}

export default Col1