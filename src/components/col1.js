import React from 'react'
import {useDispatch, useSelector } from 'react-redux'
import {initCardUIinCol} from './gameAction'
import Card from './card'

const Col1 = () => {
    const col1 = useSelector(state => state.col_1_Cards);
    const initCard = initCardUIinCol([...col1])
    console.log('render')
    return (
        <div className="card-col col-1">
            {initCard.map((card, index) => {
                return <Card key={index} card={card} style={{top: index*30}} dragCol={1} index={index}/>
            })}
        </div>
    )
}

export default Col1