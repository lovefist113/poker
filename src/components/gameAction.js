import _ from 'lodash'
export const initCardUIinCol = (col) => {
    // flip up last card
    _.set(col[col.length - 1], 'flip', true)
    return col
}

export const moveCard = (e, card, callback) => {
    console.log(card)
    var dropZone = null;
    // prepare to move
    const thisCard = e.target;
    // get mouse offset of card
    let shiftX = e.clientX - thisCard.getBoundingClientRect().left;
    let shiftY = e.clientY - thisCard.getBoundingClientRect().top;
    
    // get original state
    const orgPosY = thisCard.offsetTop;
    const orgPosX = thisCard.offsetLeft;
    const orgIndex =thisCard.style.zIndex;
    var mousePosX = e.pageX;
    var mousePosY = e.pageY;

    // START DRAGING
    // Change state 
    thisCard.style.position = 'fixed';
    thisCard.style.zIndex = 1000;

    // set card position right at the mouse position
    const moveAt = (pageX, pageY) => {
        thisCard.style.left = pageX - shiftX +'px'
        thisCard.style.top = pageY - shiftY + 'px'
    }
    //put that absolutely positioned card under the cursor
    moveAt(mousePosX, mousePosY);
    
    //move card function
    const onMouseMove = (e) => {
        moveAt(e.pageX, e.pageY);
        if(e.pageX >= 30 && e.pageX <= 144 && e.pageY >= 250 ){
            dropZone = 1
        }
        if(e.pageX >= 180 && e.pageX <= 294 && e.pageY >= 250 ){
            dropZone = 2
        }
        if(e.pageX >= 330 && e.pageX <= 444 && e.pageY >= 250 ){
            dropZone = 3
        }
        if(e.pageX >= 480 && e.pageX <= 594 && e.pageY >= 250 ){
            dropZone = 4
        }
        if(e.pageX >= 630 && e.pageX <= 744 && e.pageY >= 250 ){
            dropZone = 5
        }
        if(e.pageX >= 780 && e.pageX <= 894 && e.pageY >= 250 ){
            dropZone = 6
        }
        if(e.pageX >= 930 && e.pageX <= 1144 && e.pageY >= 250 ){
            dropZone = 7
        }
    }
    // Disable default drag event
    thisCard.ondragstart = function() {
        return false;
    };
    // move card on mouse move
    document.addEventListener('mousemove', onMouseMove);

    // drop the card,
    thisCard.onmouseup = () => {
        // remove unneeded handlers
        document.removeEventListener('mousemove', onMouseMove);
        // return card to original position
        thisCard.style.position = 'absolute';
        thisCard.style.left = orgPosX + 'px';
        thisCard.style.top = orgPosY + 'px';
        thisCard.style.zIndex = orgIndex;
        // remove event mouseup
        thisCard.onmouseup = null;
        // Callback function
        callback(dropZone)
    }
}