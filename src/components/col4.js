import React from 'react'
import {useDispatch, useSelector } from 'react-redux'
import {initCardUIinCol} from './gameAction'
import Card from './card'

const Col1 = () => {
    const col4 = useSelector(state => state.col_4_Cards);
    const initCard = initCardUIinCol([...col4]);
    return (
        <div className="card-col col-4">
            {initCard.map((card, index) => {
                return <Card key={index} card={card} style={{top: index*30}} dragCol={4} index={index}/>
            })}
        </div>
    )
}

export default Col1