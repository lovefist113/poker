import React from 'react'
import {useDispatch, useSelector } from 'react-redux'
import {shuffle, initDrawCards, initColCard} from '../actions/apiAction'
import {PlaygroundStyle} from './../style/playground.style'
import DrawDesk from './drawDesk'
import Col1 from './col1'
import Col2 from './col2'
import Col3 from './col3'
import Col4 from './col4'
import Col5 from './col5'
import Col6 from './col6'
import Col7 from './col7'
import ArchiveCard from './archiveCard'
// import _ from 'lodash'

const Playground = () => {
  const desk = useSelector(state => state.desk);
  const dispatch = useDispatch();
  // Shuffle 
  // random card in desk
  desk.sort(function(a, b){return 0.5 - Math.random()});
  // update state with shuffled desk
  dispatch(shuffle(desk))

  // init drawCards
  // get 24 card from desk
  var drawCards = desk.splice(0,24);
  dispatch(initDrawCards(drawCards,desk))

  // INIT COL CARDS
  for(let i = 1; i <= 7; i++){
    // get col cards
    let colCards = desk.splice(0,i);
    dispatch(initColCard(i,colCards,desk))
  }
  // divice card
  
  // render
  return (
    <PlaygroundStyle className="playground">
      <DrawDesk {...drawCards}/>
      <ArchiveCard {...drawCards}/>
      <Col1/>
      <Col2/>
      <Col3/>
      <Col4/>
      <Col5/>
      <Col6/>
      <Col7/>
    </PlaygroundStyle>
  )
    
  
}

export default Playground