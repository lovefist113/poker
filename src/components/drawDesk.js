import React, {useState} from 'react'
import {useSelector } from 'react-redux'
import Card from './card'
import _ from 'lodash'

const DrawDesk = () => {
    const drawcards = useSelector(state => state.drawCards)
    const [cardInDrawDesk, setcardInDrawDesk] = useState(drawcards)
    const [drawedCards, setdrawedCards] = useState([]);
    const [outOfDrawedCard, setOutOfDrawedCard] = useState(false)

    // set Style
    const styles = {
        backgroundDown: {backgroundImage: 'url("' +process.env.PUBLIC_URL+ '/images/down.png")'},
        outOfDrawedCard: {backgroundImage: 'none', border: '1px solid rgba(179, 179, 179, 0.7)'}
    }

    // Function
    // function draw card
    const getCard = () => {
        // get top card in drawDesk
        var newdrawedCardsArr = [...drawedCards];
        var _cardInDrawDesk = [...cardInDrawDesk];
        const newdrawedCard = _.head(cardInDrawDesk);
        // end drawing card and reset
        newdrawedCard && newdrawedCardsArr.push(newdrawedCard);
        setdrawedCards(newdrawedCardsArr);
        // cardInDrawDesk should remove this card
        _.remove(_cardInDrawDesk, (n) => {
            return _.isEqual(n,_.head(cardInDrawDesk))
        })
        setcardInDrawDesk(_cardInDrawDesk)
        // Change draw desk to reset desk if there is not card in desk
        _cardInDrawDesk.length === 0 && setOutOfDrawedCard(true)
    }
    // handle reset draw desk
    const resetDrawDesk = () => {
        // turn all drawed card back to draw desk
        setcardInDrawDesk(drawedCards);
        // reset drawedCard to blank
        setdrawedCards([]);
        // drawDesk should be able to draw again
        setOutOfDrawedCard(false);
    }
    // render
    const visibleDrawedCards = drawedCards.length >= 3 ? _.slice(drawedCards, drawedCards.length - 3, drawedCards.length) : drawedCards;
    return (
        <div className="draw-desk">
            <div className={`card get-card`} style={outOfDrawedCard ? styles.outOfDrawedCard : styles.backgroundDown} onClick={outOfDrawedCard ? resetDrawDesk : getCard}>
                {outOfDrawedCard && 
                <div className="reset-circle"></div>
                }
            </div>
            <div className={`drawed-card`}>
                {visibleDrawedCards.map((card, index) => {
                    _.set(card, 'flip', true)
                    return (
                        <Card key={index} card={card} style={{left: index*20, zIndex: index}} dragCol={'drawCard'}/>
                    )
                }
                )}
            </div>
        </div>
    )
}

export default DrawDesk