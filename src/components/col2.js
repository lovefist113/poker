import React from 'react'
import {useDispatch, useSelector } from 'react-redux'
import {initCardUIinCol} from './gameAction'
import Card from './card'

const Col1 = () => {
    const col2 = useSelector(state => state.col_2_Cards);
    const initCard = initCardUIinCol([...col2])
    return (
        <div className="card-col col-2">
            {initCard.map((card, index) => {
                return <Card key={index} card={card} style={{top: index*30}} dragCol={2} index={index}/>
            })}
        </div>
    )
}

export default Col1