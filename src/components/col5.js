import React from 'react'
import {useDispatch, useSelector } from 'react-redux'
import {initCardUIinCol} from './gameAction'
import Card from './card'

const Col1 = () => {
    const col5 = useSelector(state => state.col_5_Cards);
    const initCard = initCardUIinCol([...col5])
    return (
        <div className="card-col col-5">
            {initCard.map((card, index) => {
                return <Card key={index} card={card} style={{top: index*30}} dragCol={5} index={index}/>
            })}
        </div>
    )
}

export default Col1