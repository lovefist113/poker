import React from 'react'
import {useDispatch, useSelector } from 'react-redux'
import {initCardUIinCol} from './gameAction'
import Card from './card'

const Col1 = () => {
    const col7 = useSelector(state => state.col_7_Cards);
    const initCard = initCardUIinCol([...col7]);
    return (
        <div className="card-col col-7">
            {initCard.map((card, index) => {
                return <Card key={index} card={card} style={{top: index*30}} dragCol={7} index={index}/>
            })}
        </div>
    )
}

export default Col1