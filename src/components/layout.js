import React, { Component } from 'react'
import LayoutStyle from '../style/layout-style'
import Playground from './playground'

export default class layout extends Component {
  render() {
    return (
      <div>
        <LayoutStyle>
          <Playground/>
        </LayoutStyle>
      </div>
    )
  }
}
