import React from 'react';
import store from './store/store'
import { Provider } from 'react-redux'

import Layout from './components/layout'

import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  * {
    padding:0;
    margin:0;
    box-sizing: border-box
  }

  body{
    font-family: 'Roboto', sans-serif;
  }
`

function App() {
  return (
    <>
    <GlobalStyle/>
    <Provider store={store}>
      <Layout/>
    </Provider>
    </>
  );
}

export default App;
