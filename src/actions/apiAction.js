
export const shuffle = (desk) => {
  return {
    type: 'SHUFFLE_DESK',
    payload: desk
  }
}

export const initDrawCards = (drawCards, desk) => {
  return {
    type: 'INIT_DRAW_CARDS',
    drawCards: drawCards,
    desk: desk
  }
}

export const initColCard = (i, colCards, desk) => {
  return {
    type: `INIT_COL_${i}_CARDS`,
    colCards: colCards,
    desk: desk
  }
}

export const updateColCard = (i, colCards) => {
  return {
    type: `UPDATE_COL_${i}_CARDS`,
    colCards
  }
}

export const updateDragArr = (dragArr) => {
  return {
    type: `UPDATE_DRAG_ARRAY`,
    dragArr
  }
}