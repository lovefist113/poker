export default [
    {
        "number": 1,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a1"
    },
    {
        "number": 2,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a2"
    },
    {
        "number": 3,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a3"
    },
    {
        "number": 4,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a4"
    },
    {
        "number": 5,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a5"
    },
    {
        "number": 6,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a6"
    },
    {
        "number": 7,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a7"
    },
    {
        "number": 8,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a8"
    },
    {
        "number": 9,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a9"
    },
    {
        "number": 10,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a10"
    },
    {
        "number": 11,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a11"
    },
    {
        "number": 12,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a12"
    },
    {
        "number": 13,
        "type": "a",
        "flip": false,
        "color": "black",
        "name": "a13"
    },
    {
        "number": 1,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b1"
    },
    {
        "number": 2,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b2"
    },
    {
        "number": 3,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b3"
    },
    {
        "number": 4,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b4"
    },
    {
        "number": 5,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b5"
    },
    {
        "number": 6,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b6"
    },
    {
        "number": 7,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b7"
    },
    {
        "number": 8,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b8"
    },
    {
        "number": 9,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b9"
    },
    {
        "number": 10,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b10"
    },
    {
        "number": 11,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b11"
    },
    {
        "number": 12,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b12"
    },
    {
        "number": 13,
        "type": "b",
        "flip": false,
        "color": "black",
        "name": "b13"
    },
    {
        "number": 1,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c1"
    },
    {
        "number": 2,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c2"
    },
    {
        "number": 3,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c3"
    },
    {
        "number": 4,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c4"
    },
    {
        "number": 5,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c5"
    },
    {
        "number": 6,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c6"
    },
    {
        "number": 7,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c7"
    },
    {
        "number": 8,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c8"
    },
    {
        "number": 9,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c9"
    },
    {
        "number": 10,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c10"
    },
    {
        "number": 11,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c11"
    },
    {
        "number": 12,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c12"
    },
    {
        "number": 13,
        "type": "c",
        "flip": false,
        "color": "red",
        "name": "c3"
    },
    {
        "number": 1,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d1"
    },
    {
        "number": 2,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d2"
    },
    {
        "number": 3,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d3"
    },
    {
        "number": 4,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d4"
    },
    {
        "number": 5,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d5"
    },
    {
        "number": 6,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d6"
    },
    {
        "number": 7,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d7"
    },
    {
        "number": 8,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d8"
    },
    {
        "number": 9,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d9"
    },
    {
        "number": 10,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d10"
    },
    {
        "number": 11,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d11"
    },
    {
        "number": 12,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d12"
    },
    {
        "number": 13,
        "type": "d",
        "flip": false,
        "color": "red",
        "name": "d13"
    }
]