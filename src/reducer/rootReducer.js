import desk from '../store/desk'
const initialState = {
  desk:desk
};


const rootReducer = (state = initialState, action) => {
  switch (action.type){
    // INIT GAME
    case 'SHUFFLE_DESK':
      return {...state, desk: action.payload}
    case 'INIT_DRAW_CARDS':
      return {...state, drawCards: action.drawCards, desk: action.desk}  
    case 'INIT_COL_1_CARDS':
      return {...state, col_1_Cards: action.colCards, desk: action.desk}  
    case 'INIT_COL_2_CARDS':
      return {...state, col_2_Cards: action.colCards, desk: action.desk}  
    case 'INIT_COL_3_CARDS':
      return {...state, col_3_Cards: action.colCards, desk: action.desk}  
    case 'INIT_COL_4_CARDS':
      return {...state, col_4_Cards: action.colCards, desk: action.desk}
    case 'INIT_COL_5_CARDS':
      return {...state, col_5_Cards: action.colCards, desk: action.desk}     
    case 'INIT_COL_6_CARDS':
      return {...state, col_6_Cards: action.colCards, desk: action.desk}
    case 'INIT_COL_7_CARDS':
      return {...state, col_7_Cards: action.colCards, desk: action.desk} 
    // UPDATE CARD
    case 'UPDATE_COL_1_CARDS':
      return {...state, col_1_Cards: action.colCards}      
    case 'UPDATE_COL_2_CARDS':
      return {...state, col_2_Cards: action.colCards}     
    case 'UPDATE_COL_3_CARDS':
      return {...state, col_3_Cards: action.colCards}     
    case 'UPDATE_COL_4_CARDS':
      return {...state, col_4_Cards: action.colCards}  
    case 'UPDATE_COL_5_CARDS':
      return {...state, col_5_Cards: action.colCards}
    case 'UPDATE_COL_6_CARDS':
      return {...state, col_6_Cards: action.colCards}     
    case 'UPDATE_COL_7_CARDS':
      return {...state, col_7_Cards: action.colCards}   
    case 'UPDATE_DRAG_ARRAY':
      return {...state, dragArr: action.dragArr}       
    default: 
      return state 
  }
}

export default rootReducer